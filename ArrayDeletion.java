/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

/**
 *
 * @author Name
 */
import java.util.Scanner;

public class ArrayDeletion {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int[] arr = {1, 2, 3, 4, 5};
        int index = kb.nextInt();
        deleteElementByIndex(arr,index);
        int[] storage = deleteElementByIndex(arr, index);
        for(int i = 0 ; i < storage.length ; i++){
            System.out.print(storage[i] + " ");
        }
        System.out.println();
        int value = kb.nextInt();
        deleteElementByValue(deleteElementByIndex(arr, index), value);
    }
    public static int[] deleteElementByIndex(int[] arr, int index){
        int[] newArr = new int[arr.length - 1];
        int deleteIndex = index;
        for(int i =0 ; i < deleteIndex ; i++){
            newArr[i] = arr[i];
        }
        for(int j = deleteIndex+1 ; j < arr.length ; j++){
            newArr[j-1] = arr[j];
        }
        int[] arrUpdate = newArr;
        return arrUpdate;
    }
    public static void deleteElementByValue(int[] arr, int value){
        int[] newArr = new int[arr.length - 1];
        int deleteElementByValue = value;
        int newIndex = 0;
        for(int i = 0 ; i < arr.length ; i++){
            if(arr[i] == deleteElementByValue){
                i++;
            }
            newArr[newIndex] = arr[i];
            newIndex++;
        }
        for(int i = 0 ; i < newArr.length ; i++){
            System.out.print(newArr[i] + " ");
        }
        System.out.println();
    }

}

